import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';
import { HomecontentComponent } from './homecontent/homecontent.component';
import { HomeheaderComponent } from './homeheader/homeheader.component';
import { HomefooterComponent } from './homefooter/homefooter.component';

@NgModule({
  imports: [
    CommonModule,
    HomepageRoutingModule
  ],
  declarations: [
    HomecontentComponent,
    HomeheaderComponent,
    HomefooterComponent
  ]
})
export class HomepageModule { }
