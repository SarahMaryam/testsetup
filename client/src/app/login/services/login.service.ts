import { Injectable, EventEmitter } from '@angular/core';

import { CaseMangementHttpClient } from '../../core/http.client';
import { WebStorage } from '../../core/web.storage';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { appConfig } from "../../core/app.config";


@Injectable()
export class LoginService {
  isLoggedIn: boolean = false;
  isAdminLoggedIn: boolean = false;
  onLoggedOut: EventEmitter<boolean> = new EventEmitter();
  onLoggedIn: EventEmitter<boolean> = new EventEmitter();
  redirectUrl: string = '';

  constructor(
    private http: CaseMangementHttpClient,
    private storage: WebStorage
  ) { }

  // getAdminDashboardList(data: any): Observable<any> {
  //   return this.http.get('/hello', data);
  // }

  userLogin(data: any): Observable<any> {
        this.storage.clearAll();
        return this.http.post('/userLogin', data).pipe(map((result: any) => {
            debugger
            if (result.code == 200) {

                    this.isLoggedIn = true;
                    if (data.rememberme == true) {
                        this.storage.localStore(appConfig.storage.TOKEN, result.data.token);
                        this.storage.localStore(appConfig.storage.ID, result.data._id);
                        this.storage.localStore(appConfig.storage.USERNAME, result.data.username);
                        this.storage.localStore(appConfig.storage.USERFNAME, result.data.firstname);
                        this.storage.localStore(appConfig.storage.USERLNAME, result.data.lastname);
                        this.storage.localStore(appConfig.storage.PROFILE_PIC, result.data.image);
                    } else {
                        this.storage.sessionStore(appConfig.storage.TOKEN, result.data.token);
                        this.storage.sessionStore(appConfig.storage.ID, result.data._id);
                        this.storage.sessionStore(appConfig.storage.USERNAME, result.data.username);
                        this.storage.sessionStore(appConfig.storage.USERFNAME, result.data.firstname);
                        this.storage.sessionStore(appConfig.storage.USERLNAME, result.data.lastname);
                        this.storage.sessionStore(appConfig.storage.PROFILE_PIC, result.data.image);
                    }

                    this.storage.sessionStore(appConfig.storage.USERNAME, result.data.username);
                    this.storage.sessionStore(appConfig.storage.USERFNAME, result.data.firstname);
                    this.storage.sessionStore(appConfig.storage.USERLNAME, result.data.lastname);
                    localStorage.setItem('isLoggedin', 'true');

                    this.onLoggedIn.emit(true);
                    
                return result;
            }
            else {
                this.isLoggedIn = false;
                return result;
            }
        }));

    }

    logout(): void {
        this.isLoggedIn = false;
        this.storage.clearAll();
        this.storage.clear(appConfig.storage.USER);
        this.onLoggedOut.emit(true);
        localStorage.removeItem('isLoggedin');
    }


}
