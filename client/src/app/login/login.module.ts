import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { Ng2Webstorage } from 'ngx-webstorage';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    imports: [
        CommonModule, 
        LoginRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        Ng2Webstorage,
        // BrowserAnimationsModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
