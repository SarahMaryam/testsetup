import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable()
export class CaseMangementHttpClient {

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {}

  createAuthorizationHeader(headers: HttpHeaders) {
    headers.append('Content-Type', 'application/json; charset=utf-8');
    // headers.append('Authorization', 'bearer ' + '');
  } 

  get(url:any, data:any, fullUrl?:any) {
    fullUrl = false;
    let headers = new HttpHeaders();
    this.createAuthorizationHeader(headers);
    let options:any = {
      headers: headers
    };
    let params: HttpParams = new HttpParams();
    Object.keys(data).map(function(key, index) { 
      if(data[key] != ''){
        params.set(key, data[key]);
      }         
    });      
    options['search'] = params;
    let reqUrl = (fullUrl)?url:environment.apiUrl+url;
    return this.http.get(reqUrl, options)
  }

  post(url:any, data:any, fullUrl?:any,contentType?:any) { 
    fullUrl = fullUrl || false;
    let headers:any = new HttpHeaders();
    this.createAuthorizationHeader(headers);
    let reqUrl = (fullUrl)?url:environment.apiUrl+url;
    let options:any = {
      headers: headers
    };
    return this.http.post(reqUrl, data, options);
  }

  extractData(res: any) {
    let body = res.json();
    return body.data || { };
  }

  handleError (error: any) {
    let errMsg: string= error.message ? error.message : error.toString();
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}