export const appConfig = {    
    storage: {
        'USER': '_wc_cu_',
        'USERNAME': '_wc_cuname_',
        'PROFILE_PIC': '_wc_cu_profile_pic',
        'TOKEN': '_wc_ct_',
        'ID': '_wc_ci_',
        'USERFNAME': '_wc_cufs_',
        'USERLNAME': '_wc_culs_',
    },
    pattern: {
        'NAME': /^[a-zA-Z . \-\']*$/,
        "CITY": /^[a-zA-Z . \-\']*$/,
        "EMAIL": /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "POSTAL_CODE": /(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$)/, // /(^\d{5}$)|(^\d{5}-\d{4}$)/,
        "PHONE_NO": /\(?\d{3}\)?-? *\d{3}-? *-?\d{4}/,
        "PASSWORD": /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,}/,
        "DESCRIPTION": /^[ A-Za-z0-9_@./#&+-,]*$/,
        "TASK_CODE": /^[0-9999]{1,4}$/,
        "SUB_DOMAIN": /^[/a-z/A-Z][a-zA-Z0-9-]*[^/-/./0-9]$/,
        "PHONE_NO_MASK": ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
        "IVR_ACTION_KEY": /^[0-9]*$/,
        "IVR_NUMBER": /^[0-9]*$/,
        "RADIUS": /^[0-9]*(?:.)([0-9])+$/,
        "LATLONG": /^\s*(\-?\d+(\.\d+)?)$/,
        "SSN": /^((\d{3}-?\d{2}-?\d{4})|(X{3}-?X{2}-?X{4}))$/,
        "SSN_MASK": [/\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
        "PRACTICE_PASSWORD": /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{6,})/,
        "USERNAME": /^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){4,18}[a-zA-Z0-9]$/,
        "USERNAME_MIN_SIZ": /^[a-zA-Z0-9_](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9_]){4,18}[a-zA-Z0-9_]$/,
        "WICARE_USERNAME": /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{1,}/,
        "YEAR_MASK": /d{4}/,
        "DECIMAL": /\d+(\.\d{1,2})?/
    }
  
};
