import { Injectable, EventEmitter } from '@angular/core';
import { CaseMangementHttpClient } from '../../core/http.client';
import { WebStorage } from '../../core/web.storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { appConfig } from "../../core/app.config";


@Injectable()
export class SignUpService {

  constructor(
    private http: CaseMangementHttpClient,
    private storage: WebStorage
  ) { }

  userRegistration(data: any): Observable<any> {
    return this.http.post('/userRegistration', data);
  }



}
