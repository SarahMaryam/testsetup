import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { SignUpService } from './services/signup.service';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
        SignUpService
    ],
  declarations: [SignupComponent]
})
export class SignupModule { }
