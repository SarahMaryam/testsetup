'use strict';
var constant = require('./constants');
var mongoose = require('mongoose');
var fs = require("fs");
var commonQuery = {};

/**
 * Function is use to Fetch Single data
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
*/
commonQuery.findoneData = function findoneData(model, cond, fetchVal) {
    return new Promise(function (resolve, reject) {
        model.findOne(cond, fetchVal, function (err, userData) {
            let tempObj = {
                status: false
            }
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }

        });
    })

}

/**
 * Function is use to Fetch all data
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.findData = function findData(model, cond, fetchVal) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        
        model.find(cond, fetchVal, function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}



/**
 * Function is use to Fetch all data in descending order by date
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 8 Nov 2018
 */
commonQuery.findDataBySort = function findDataBySort(model, cond, fetchVal) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond, fetchVal).sort({ createdAt: 'descending' }).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}


/**
 * Function is use to Fetch Multiple data with populate
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 4 Nov 2018
 */
commonQuery.findDataWithPopulate = function findDataWithPopulate(model, cond, populate) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond).populate(populate).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}


/**
 * Function is use to Fetch Multiple data with multiple populate
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Mar 2019
 */
commonQuery.findDataWithMultiplePopulate = function findDataWithMultiplePopulate(model, cond, populate1, populate2) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond).populate(populate1).populate(populate2).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}

/**
 * Function is use to Fetch Multiple data with populate and limited count
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 18 Dec 2018
 */
commonQuery.findDataWithPopulateWithCount = function findDataWithPopulateWithCount(model, cond, populate, count) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond)
            .limit(parseInt(count))
            .populate(populate)
            .sort({ updatedAt: 'descending' })
            .exec(function (err, userData) {
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Fetch Multiple data with populate and limited count as per createdAt descending order
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 27 feb 2019
 */
commonQuery.findDataWithPopulateWithCountDescending = function findDataWithPopulateWithCountDescending(model, cond, populate, count) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond)
            .limit(parseInt(count))
            .populate(populate)
            .sort({ createdAt: 'descending' })
            .exec(function (err, userData) {
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Fetch Multiple data with populate and limited count as per createdAt descending order for pagination
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 11 Mar 2019
 */
commonQuery.findDataWithPopulateWithCountandPaginateDescending = function findDataWithPopulateWithCountandPaginateDescending(model, cond, populate, populate2, count, skip) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }

        // User.find(condition)
        // .limit(parseInt(count))
        // .skip(parseInt(skip))
        // .sort(sorting)
        // .lean()

        model.find(cond)
            .limit(parseInt(count))
            .populate(populate)
            .populate(populate2)
            .skip(parseInt(skip))
            .sort({ createdAt: 'descending' })
            .exec(function (err, userData) {
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Fetch Multiple data and limited count
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 27 feb 2019
 */
commonQuery.findDataWithCount = function findDataWithCount(model, cond, count) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond)
            .limit(parseInt(count))
            .sort({ createdAt: 'descending' })
            .exec(function (err, userData) {
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Fetch Single data with populate
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.findoneDataWithPopulate = function findoneDataWithPopulate(model, cond, fetchVal, populate) {
    return new Promise(function (resolve, reject) {
        model.findOne(cond, fetchVal).populate(populate).exec(function (err, userData) {
            let tempObj = {
                status: false
            }
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}

/**
 * Function is use to Fetch Single data with populate
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 4 Jan 2019
 */
commonQuery.findoneWithPopulate = function findoneWithPopulate(model, cond, populate) {
    return new Promise(function (resolve, reject) {
        model.findOne(cond).populate(populate).exec(function (err, userData) {
            let tempObj = {
                status: false
            }
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}

/**
 * Function is use to Fetch Single data with populate in descending order
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Nov 2018
 */
commonQuery.findoneDataWithPopulateInDesc = function findoneDataWithPopulateInDesc(model, cond, populate) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.findOne(cond).sort({ createdAt: 'descending' }).populate(populate.path).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}



/**
 * Function is use to Fetch Single data
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.findById = function findById(model, cond) {
    return new Promise(function (resolve, reject) {
        model.findById(cond, function (err, userData) {
            if (err) {
                reject(err);
            } else {
                resolve(userData);
            }

        });
    })
}
/**
 * Function is use to Fetch Single data
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.updatedById = function updatedById(model) {
    return new Promise(function (resolve, reject) {
        model.save(function (err, userData) {
            if (err) {
                reject(err);
            } else {
                resolve(userData);
            }
        });
    })
}
/**
 * Function is use to Last Inserted id
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.lastInsertedId = function lastInsertedId(model) {
    return new Promise(function (resolve, reject) {
        model.findOne().sort({
            id: -1
        }).exec(function (err, data) {
            if (err) {
                resolve(0);
            } else {
                if (data) {
                    var id = data.id + 1;
                } else {
                    var id = 1;
                }
            }
            resolve(id);
        });
    })
}

/**
 * Function is use to Insert object into Collections
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 26 Oct 2018
 */
commonQuery.InsertIntoCollection = function InsertIntoCollection(model, obj) {
    return new Promise(function (resolve, reject) {
        new model(obj).save(function (err, userInfo) {
            let tempObj = {
                status: false
            }
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userInfo;
                resolve(tempObj);
            }
        });
    })
}

/**
 * Function is use to Insert many into Collections
 * @access private
 * @return json
 * Created by prakash
 * @smartData Enterprises (I) Ltd
 * Created Date 18 Apr 2019
 */
commonQuery.InsertManyIntoCollection = function InsertManyIntoCollection(model, arr) {
    return new Promise(function (resolve, reject) {
        try {
            model.insertMany(arr,function (err, userInfo) {
                let tempObj = {
                    status: false
                }
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userInfo;
                    resolve(tempObj);
                }
            });
        } catch (error) {
            console.log(" on insert many ::userInfo::>", error);
        }

    })
}
/**
 * Function is use to upload file into specific location
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 31-Jan-2018
 */
commonQuery.fileUpload = function fileUpload(imagePath, buffer) {
    return new Promise((resolve, reject) => {
        try {
            let tempObj = {
                status: false
            }
            fs.writeFile(imagePath, buffer, function (err) {
                if (err) {
                    tempObj.error = err;
                    reject(err);
                } else {
                    tempObj.status = true;
                    tempObj.message = 'uploaded';
                    resolve(tempObj);
                }
            });
        } catch (e) {
            reject(e);
        }
    });
}
/**
 * Function is use to check File Exist or not
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Jan-2018
 */
commonQuery.FileExist = function FileExist(imagePath, noImage, imageloc) {
    return new Promise(function (resolve, reject) {
        utility.fileExistCheck(imagePath, function (exist) {
            if (!exist) {
                resolve(constant.config.baseUrl + noImage);
            } else {
                resolve(constant.config.baseUrl + imageloc);
            }
        });
    })
}
/**
 * Function is use to delete file from specific directory
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 31-Jan-2018
 */
commonQuery.deleteFile = function deleteFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.unlink(filePath, function (err) {
            if (err) {
                reject(err);
            } else {
                resolve("success");
            }
        });
    })
}

/**
 * Function is use to Update One Document
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Jan-2018
 */
commonQuery.updateOneDocument = function updateOneDocument(model, updateCond, userUpdateData) {
    return new Promise(function (resolve, reject) {
        // let obj = { status: false };
        model.findOneAndUpdate(updateCond, {
            $set: userUpdateData
        }, {
                new: true
            })
            .lean().exec(function (err, userInfoData) {
                let tempObj = {
                    status: false
                }
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userInfoData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Update All Document
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 27-Dec-2018
 */
commonQuery.updateAllDocument = function updateAllDocument(model, updateCond, userUpdateData) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.update(updateCond, {
            $set: userUpdateData
        }, {
                multi: true
            })
            .lean().exec(function (err, userInfoData) {
                if (err) {
                    tempObj.error = err;
                    reject(tempObj);
                } else {
                    tempObj.status = true;
                    tempObj.data = userInfoData;
                    resolve(tempObj);
                }
            });
    })
}

/**
 * Function is use to Find all Documents
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Jan-2018
 */
commonQuery.fetch_all = function fetch_all(model, cond, fetchd) {
    return new Promise(function (resolve, reject) {
        model.find(cond, fetchd).exec(function (err, userData) {
            if (err) {
                reject(err);
            } else {
                resolve(userData);
            }

        });
    })
}

/**
 * Function is use to Count number of record from a collection
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Jan-2018
 */
commonQuery.countData = function countData(model, cond) {
    let tempObj = { status: false }
    return new Promise(function (resolve, reject) {
        model.count(cond).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }

        });
    })
}
/**
 * Function is use to Fetch All data from collection , Also it supports aggregate function
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 23-Jan-2018
 */
commonQuery.fetchAllLimit = function fetchAllLimit(query) {
    return new Promise(function (resolve, reject) {
        query.exec(function (err, userData) {
            if (err) {
                reject(err);
            } else {
                resolve(userData);
            }
        });
    })
}

/**
 * Function is use to Insert object into Collections , Duplication restricted
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 07-Feb-2018
 */
commonQuery.uniqueInsertIntoCollection = function uniqueInsertIntoCollection(model, obj) {
    let result = { status: false }
    return new Promise(function (resolve, reject) {
        new model(obj).save(function (err, userData) {
            if (err) {
                result.err = err
                reject(result);
            } else {
                result.status = true
                result.userData = userData
                resolve(result);
            }
        });
    })
}


/**
 * Function is use to DeleteOne Query
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 07-Feb-2018
 */
commonQuery.deleteOneDocument = function deleteOneDocument(model, cond) {
    let result = { status: false }

    return new Promise(function (resolve, reject) {
        model.deleteOne(cond).exec(function (err, userData) {
            if (err) {
                result.err = err
                reject(result);
            } else {
                result.status = true
                result.userData = userData
                resolve(result);
            }

        });
    })
}

/**
 * Function is use to delete Many document from Collection
 * @access private
 * @return json
 * Created by Divya
 * @smartData Enterprises (I) Ltd
 * Created Date 16-Feb-2018
 */
commonQuery.deleteManyfromCollection = function deleteManyfromCollection(model, obj) {
    return new Promise(function (resolve, reject) {
        model.deleteMany(obj, function (error, inserted) {
            if (error) {
                resolve(0);
            } else {
                resolve(1);
            }

        });
    })
}

/**
 * Function is use to Fetch all availability data in ascending order by startDate
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 9 April 2019
 */
commonQuery.findAvailabilityDataBySort = function findAvailabilityDataBySort(model, cond) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond).sort({ startDate: 1 }).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}

/**
 * Function is use to Fetch all availability data in descending order by startDate
 * @access private
 * @return json
 * Created by Saurabh
 * @smartData Enterprises (I) Ltd
 * Created Date 9 April 2019
 */
commonQuery.findAvailabilityDataBySortDescending = function findAvailabilityDataBySortDescending(model, cond) {
    return new Promise(function (resolve, reject) {
        let tempObj = {
            status: false
        }
        model.find(cond).sort({ endDate: -1 }).exec(function (err, userData) {
            if (err) {
                tempObj.error = err;
                reject(tempObj);
            } else {
                tempObj.status = true;
                tempObj.data = userData;
                resolve(tempObj);
            }
        });
    })
}

commonQuery.dateToStringData = function dateToStringData(model, condi) {
    return new Promise((resolve, reject) => {
        model.aggregate(condi
        ).exec(function (err, data) {
            if (err) {
                console.log('reject error ', err)
                reject(err);
            } else {
                resolve(data);
            }

        })
    })
}


// commonQuery.numberOfAssociatedUser = function numberOfAssociatedUser(model, condi) {
//     return new Promise((resolve, reject) => {
//         model.aggregate([
//             {$match: { userType: constant.UserTypes.staff, created_by_id: condi.created_by_id} },
//             {$project: { createdAt:1, email: 1, day: {$dayOfMonth: '$createdAt'}}},
//             {$match: {day: condi.day}},
//         ]).exec(function (err, data) {
//             if (err) {
//                 reject(err);
//             } else {
//                 resolve(data);
//             }

//         })
//     })
// }


commonQuery.findoneBySort = function findoneBySort(model, condition, fetchVal, sortby) {
    return new Promise(function (resolve, reject) {
      
        if (!sortby) {
            sortby = {
                _id: -1
            };
        }
        model.findOne(condition, fetchVal).sort(sortby).exec(function (err, data) {
            if (err) {
                console.log('err---->>>>>', err);
                reject(err);
            } else {
                resolve(data);
            }
        });
    })
}


commonQuery.countStaffData = function countStaffData(model, cond) {
    return new Promise(function (resolve, reject) {
        model.countDocuments(cond).exec(function (err, userData) {
            if (err) {
                reject(err);
            } else {
                resolve(userData);
            }

        });
    })
}

commonQuery.FindOne = function FindOne(model, cond = {}, projection = {}) {
    return new Promise((resolve, reject) => {
        try {
            if (!model || !Object.keys(cond).length)
                return reject({
                    statusCode: 400,
                    message: !model ? 'table' : 'condition to find'
                });
            model.findOne(cond, projection).exec(function (err, data) {
                if (err) {
                    console.log(err);
                    return reject(err);
                } else {
                    console.log(data);
                    return resolve(data);
                }

            });
        } catch (error) {
            console.log(error);
            return reject({
                statusCode: 500,
                message: 'Internal server error'
            });
        }
    });
}

commonQuery.mongoObjectId = function (data) {
    if (data && data !== null && data !== undefined) {
        return mongoose.Types.ObjectId(data);
    } else {
        return false;
    }
}

module.exports = commonQuery;
