var mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    constant = require('./constants'),
    async = require("async"),
    nodemailer = require('nodemailer'),
    ejs = require('ejs'),
    moment = require('moment'),
    path = require('path'),
    smtpTransport = require('nodemailer-smtp-transport');
    // EmailTemplate = require('../api/v1/modules/email_template/models/email_template_models');
const config = require('../config/config.js').get(process.env.NODE_ENV || 'local');

const AWS = require('aws-sdk')
AWS.config.update({
    accessKeyId: config.aws_ses.accessKeyId,
    secretAccessKey: config.aws_ses.secretAccessKey,
    region: config.aws_ses.region,
    // ses: '2019-01-17',
    ses: '2019-01-18',
    
});
const ses = new AWS.SES();

module.exports = {
    // sendMail: sendMail,
    // cronJobNotification: cronJobNotification

};


var transporter = nodemailer.createTransport(smtpTransport({
    service: config.smtp.service,
    port: 465,
    secure: true,
    host: config.smtp.host,
    auth: {
        user: config.smtp.username,
        pass: config.smtp.password
    },
    tls: {
        rejectUnauthorized: false
    }
}));


// function sendMail(to, keyword, userData, callbackMail) {
//     EmailTemplate.findOne({ 'unique_keyword': keyword, isDeleted: false }, function (err, template) {
//         if (err) {
//             callbackMail(err, null);
//         } else {
//             replacePlaceholders(userData, template.description, template.subject, function (mailContent, subject) {
//                 var options = { mailbody: mailContent }
//                 generateTemplate(options, function (mailContent) {
                    
//                     var mailOptions = {
//                         from: config.smtp.mailUsername, // sender address
//                         to: to, // list of receivers
//                         subject: subject, // Subject line
//                         html: mailContent // Mail content body 
//                     };

//                     if(config.env == 'local' || config.env == 'staging'){
//                         //local and staging
//                         transporter.sendMail(mailOptions, function (error, info) { // send mail with defined transport object
//                             if (error) {
//                                 callbackMail(error, null);
//                             } else {
                                
//                                 var returnMsg = 'Mail sent successfully';
//                                 callbackMail(null, { message: returnMsg });
//                             }
//                         });
//                     }
//                     else{
//                     //Live
//                         const params = {
//                             Destination: {
//                                 ToAddresses: [to]
//                             },
//                             Message: {
//                                 Body: {
//                                     Html: {
//                                         Charset: 'UTF-8',
//                                         Data: mailOptions.html
//                                     }
//                                 },
//                                 Subject: {
//                                     Charset: 'UTF-8',
//                                     Data: mailOptions.subject
//                                 }
//                             },
//                             ReturnPath: config.aws_ses.fromName,
//                             Source: config.aws_ses.fromName
//                         }

//                         ses.sendEmail(params, (err, data) => {
//                             if (err) {
//                                 callbackMail(err,null);
//                             } else {
                                
//                                 var returnMsg = 'Mail sent successfully';
//                                 callbackMail(null, { message: returnMsg });
//                             }
//                         })
//                     }
//                 });
//             })


//         }
//     })
// }

/* @function : generateTemplate
 *  @created  : 24/08/2018
 *  @modified :
 *  @purpose  : Create layout for emails header and footer
*/
var generateTemplate = function (options, callbackg) {
    var recepient = options.recepient || '',
        mailbody = options.mailbody;

    var fileName = path.resolve('./api/lib/mailTemplate.html');
    ejs.renderFile(fileName, { recepient: recepient, mailbody: mailbody }, {}, function (err, str) {
        if (err) {
            callbackg(mailbody);
        } else {
            callbackg(str || mailbody);
        }
    });
}
var currYear = new Date().getFullYear();
var MDoutUrl = config.baseUrl

var replacePlaceholders = function (data, mailbody, subj, callbackr) {
    var content = mailbody.replace(/\[\[(.*?)\]\]/g, function (match, token) {
        switch (token) {
            case 'BASEURL':
                return config.baseUrl;
                break;
            case 'FirstName':
                return data.firstName.charAt(0).toUpperCase() + data.firstName.slice(1).toLowerCase() ? data.firstName.charAt(0).toUpperCase() + data.firstName.slice(1).toLowerCase() : '';
                break;
            case 'LastName':
                return data.lastName.charAt(0).toUpperCase() + data.lastName.slice(1).toLowerCase();
                break;
            case 'Password':
                return data.password;
                break;
            case 'Email':
                return data.email;
                break;
            case 'verifingLink':
                return "<a style ='border-bottom:1px solid #d7d7d7;font-size: 16px;background:#028dd8;color:#fff;text-align:center;padding: 10px 20px 7px;font-weight:normal;display: inline-block;margin: 10px 0;text-decoration: none;' href='" + data.verifingLink + "' target='_blank'>Verify Email Address</a>";
                break;
            case 'loginLink':
                return "<a style ='border-bottom:1px solid #d7d7d7;font-size: 16px;background:#028dd8;color:#fff;text-align:center;padding: 10px 20px 7px;font-weight:normal;display: inline-block;margin: 10px 0;text-decoration: none;' href='" + data.loginLink + "' target='_blank'>Check newly added content</a>";
                break;
            case 'assignSurveyLink':
                return "<a style ='border-bottom:1px solid #d7d7d7;font-size: 16px;background:#028dd8;color:#fff;text-align:center;padding: 10px 20px 7px;font-weight:normal;display: inline-block;margin: 10px 0;text-decoration: none;' href='" + data.assignSurveyLink + "' target='_blank'>Click Here To Start</a>";                
                break;
            case 'Link':
                return data.link
                break;
            case 'SenderName':
                return data.senderName;
                break;
            case 'MDoutUrl':
                return MDoutUrl;
                break;
            case 'currYear':
                return currYear;
                break;
            case 'Password':
                return data.password;
                break;
            case 'Name':
                return data.name;
                break;
            case 'MessageBody':
                return data.message;
                break;
            case 'SurveyName':
                return data.surveyName;
                break;
            case 'ClinicName':
                return data.clinicName;
                break;
            case 'PhoneNumber':
                return data.phoneNumber;
                break;
            case 'ClinicEmail':
                return data.clinicEmail;
                break;
            case 'ClinicLogo':
                return data.clinicLogo;
                break;
            case 'BackendBASEURL':
                return config.backendBaseUrl;
                break;
            case 'physicianName':
                return data.physicianName;
                break;
            case 'patientName':
                return data.patientName;
                break;
            case 'appointMentDate':
                return data.appointMentDate;
                break;
            case 'startTime':
                return data.startTime;
                break;
            case 'endTime':
                return data.endTime;
                break;
            case 'URL':
                return data.url;
                break;
            case 'contenet':
                return data.contenet;
                break;
            case 'CouponCode':
                return data.coupon_code;
                break;
        }
    })
    var subject = subj.replace(/\[\[(.*?)\]\]/g, function (match1, token1) {
        switch (token1) {
            case 'ReceiverFname':
                return data.firstName;
                break;
            case 'ReceiverLname':
                return data.lastName
                break;
        }
    })
    if (content && subject) {
        callbackr(content, subject);
    }
}



function cronJobNotification(obj) {
                      
    const mailOptions = {
        from: config.smtp.mailUsername, // sender address
        to: obj.to, // list of receivers
        subject: obj.subject, // Subject line
        html: obj.html
    };

    if(config.env == 'local' || config.env == 'staging'){
        //local and staging
        transporter.sendMail(mailOptions, function (error, info) { // send mail with defined transport object
            if (error) {
            } else {
                
            }
        });
    }
    else{
    //Live
        const params = {
            Destination: {
                ToAddresses: [to]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: 'UTF-8',
                        Data: mailOptions.html
                    }
                },
                Subject: {
                    Charset: 'UTF-8',
                    Data: mailOptions.subject
                }
            },
            ReturnPath: config.aws_ses.fromName,
            Source: config.aws_ses.fromName
        }

        ses.sendEmail(params, (err, data) => {
            if (err) {
            } else {
                
            }
        })
    }
}