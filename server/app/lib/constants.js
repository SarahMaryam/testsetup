
const statusCode = {
    "ok": 200,
    "error": 401,
    "warning": 404,
    "failed": 1002,
    "invalidURL": 1001,
    "unauth": 402,
    "internalError": 1004
}
const messages = {
    "commonError": "Something went wrong",
    "roleAlreadyExist": "Role already exist",
    "roleAddSuccess": "Role added successfully",
    "roleUpdateSuccess": "Role updated successfully",
    "roleNotAdded": "Role cannot be added",
    "roleNameEmpty": "Role name cannot be empty",
    "roleAlreadyDeleted": "Role is either deleted or not found",
    "roleDeleteSuccess": "Role is successfully deleted",
    "roleListing": "Roles list has been successfully fetched",
    "roleDetails": "Role details has been successfully fetched",
    "permissionAssigned": "Permission has been successfully assigned",

    "PermissionDeleteSuccess": "Permission is successfully deleted",
    "permissionAlreadyDeleted": "Permission is either deleted or not found",
    "requestNotProcessed": "Request could not be processed. Please try again.",
    "channelCreated": "Channel Created Sucessfully",
    "permisssionUpdated": "Permission updated Successfully",
    "dataListing": "Data has been successfully listed",

    "dataRetrievedSuccess": "Data retrieved successfully",
    "emailAlreadyExist": "Email already exist",
    "noRecordFound": "No Record Found",
    "resetPasswordTokenExpire": "Your link has been Expired",
    "userIdRequired": "User Id is required",
    "passwordMatched": "Password matched",
    "changePasswordSuccess": "Password changed successfully",
    "passwordNotMatched": "Old password is incorrect",

    "contentSuccess": "Content list fetched successfully",
    "clinicListSuccess": "Clinic list fetched successfully",
    "clinicAddSuccess": "Clinic added successfully",
    "clinicAlreadyExist": "Clinic already exist",
    "clinicNotFound": "Clinic not found",
    "clinicUpdateSuccess": "Clinic updated successfully",
    "clinicStatusUpdateSuccess": "Clinic status updated successfully",
    "clinicDeleteSuccess": "Clinic deleted successfully.",

    "userNotactivated": "Your account not activated yet",
    "userDeactivated": "Your account has been Deactivated ",
    "userAccountActivated": "Your account has been successfully verified",
    "userAccountdeleted": "Your account has been deleted",
    "forgotPasswordSuccess": "Your reset password link has been sent successfully, please check your mail",
    "forgotPasswordError": "Not able to send link to this email address ",
    "passwordSentSuccess": "Your password has been sent successfully, please check your mail",
    "resetPasswordSuccess": "Password reset successful",
    "emailNotExist": "Email doesn't exist",
    "currentPasswordWorng": "Please provide correct current password",
    "successInChangePassword": "Password changed successfully",
    "userNotFound": "User not found",
    "userUpdated": "Profile updated successfully",
    "userList": "List fetched successfully",
    "requestList": "Request List fetched successfully",

    "loginSuccess": "Logged in successfully",
    "signupSuccess": "Signup successfully, please check you email to activate your account",
    "loginOut": "Logout successfully",

    "invalidLoginInput": "User email or password are not correct",

    "tagAddSuccess": "Tag added successfully",
    "tagDeleteSuccess": "Tag deleted successfully",

    "emailTemplateAddSuccess": "Email template added successfully",
    "emailTemplateAlreadyExist": "Email template already exist",
    "emailTemplateNotFound": "Email template not found",
    "emailTemplateUpdateSuccess": "Email template updated successfully",
    "emailTemplateStatusUpdateSuccess": "Email template status updated successfully",
    "emailTemplateDeleteSuccess": "Email template deleted successfully.",

    "groupAlreadyExist": "Group already exist",
    "groupAddSuccess": "Group created successfully",
    "groupListing": "Group list has been successfully fetched",
    "memberChannelCreatedSuccess": "Member channel created successfully",
    "createInviteSuccess": "Invite created sucessfully",
    "messageDeletedSuccess": "Message deleted sucessfully",
    "channelDeletedSuccess": "Channel deleted sucessfully",
    "memberAddSucess": "Member added to channel successfully",


    "userAddSuccess": "User added successfully",
    "userAlreadyExist": "User already exist",
    "userNotFound": "User not found",
    "userDetails": "User details found",
    "userUpdateSuccess": "User updated successfully",
    "patientUpdateSuccess": "Patient updated successfully",
    "profileUpdateSuccess": "Profile updated successfully",
    "userStatusUpdateSuccess": "User status updated successfully",
    "userAddedSuccess": "User added successfully.",
    "patientAddedSuccess": "Patient added successfully.",
    "userDeleteSuccess": "User deleted successfully.",
    "patientDeleteSuccess": "Patient deleted successfully.",
    "userPermissionUpdateSuccess": "User permission updated successfully",

    "surveyAddSucess": "Survey created successfully",
    "surveyListSucess": "Survey list fetched successfully",
    "surveyFillSucess": "Thank you for filling the survey, survey is filled successfully",
    "surveyDeleteSuccess": "Survey deleted successfully",

    "assignSurveySucess": "Survey is assigned successfully & survey link is sent to email",
    "carddetails" : "Please Enter valid card Details",

    "contentAddSuccess": "Content added successfully",
    "contentAddedToHistory": "Content added to History successfully",
    "contentDeleteSuccess": "Content deleted successfully.",
    "contentUpdateSuccess": "Content updated successfully",
    "contentPublishSuccess": "Content Published successfully",
    "contentPreventFromPublish": "Content Prevented from Publishing successfully",
    "contentListing": "Contents list has been successfully fetched",
    "topicDetails": "Topic details has been successfully fetched",
    "assignContentSuccess": "Content is successfully assigned",

    "providerAssigned": "Provider assigned successfully",
    "notificationSent": "Notification sent successfully",

    "adsAddSuccess": "Ad added successfully",
    "adsDeleteSuccess": "Ad deleted successfully.",
    "multipleAddSuccess": "Multiple Ads added successfully",

    "surveyUpdateSuccess": "Survey updated successfully",
    "assignedSurveyListSuccess": "Assigned surveys list fetched successfully",
    "assignSurveyToStaffSuccess": "Survey assigned to staff successfully",

    "notesAddSuccess": "Notes added successfully",
    "listNotesSuccess": "Notes list fetched successfully",
    "noteDeleteSuccess": "Note deleted successfully",
    "noteDetailsSuccess": "Note details fetched successfully",
    "noteEditSuccess": "Note updated successfully",

    "templateListing": "Templates list has been successfully fetched",
    "templateDetails": "Template details has been successfully fetched",
    "templateUpdateSuccess": "Template updated successfully",
    "FillQuestionaryListing": "Fill questionary list has been successfully fetched",
    "Registration": "You have successfully registered",
    "assignToStaffSuccess": "You have successfully assigned staff for view survey",
    "assignToStaffFail": "Error while assigning staff to view survey",
    "youtubeCheckUrl1": "https://www.youtube.com",
    "youtubeCheckUrl2": "https://youtu.be",
    "youtubeUrl1": "https://www.youtube.com/watch?v=",
    "youtubeUrl2": "https://youtu.be/",
    "groupCreated": "Group created sucessfully",
    "mailSentSuccess": "Mail sent Successfully",
    "notificationList": "Notification Listed successfully",
    "locationAddedSucess": "Location details saved successfully",
    "locationAlreadyExist": "Clinic Location already added",
    "locationEditSucess": "Location is updated",
    "reqAccepted": "Request accepted successfully",
    "reqRejected": "Request rejected successfully",
    "locationDeleted": "Location removed successfully",
    "userLocationDeleted": "User Location removed successfully",
    "addedClinicDeleted": "Associated clinic removed successfully",
    "locationList": "Location list fetched successfully",
    "addedClinicList": "Added clinic list fetched successfully",
    "userlocationList": "User Location list fetched successfully",
    "userAssociatedWithlocationAddedSucess": "User associated with location added successfully",
    "addAssignCategory": "Survey category created sucessfully",
    "updatedAssignCategory": "Survey category updated sucessfully",
    "deletedAssignCategory": "Survey category deleted sucessfully",
    "pushSubcriptionAlreadyExist": "Push subcriber already exist",
    "invitationSendSucess": "Invitation send sucessfully",
    "particpantList": "Participant list fetched successfully",
    "deleteMember": "Member removed sucessfully",
    "subcriptionSucess": "subcription done",
    "addClinicCategory": "Clinic category created sucessfully",
    "updatedClinicCategory": "Clinic category updated sucessfully",
    "deletedClinicCategory": "Clinic category deleted sucessfully",
    "assignCategoryAlreadyExist": "Clinic category already exist",
    "regitrationCentent": "We're excited to have you get started. First, you need to confirm your account. Just press the button below.\n\n",
    "availabilityAddedSucess": "Availability details saved successfully",
    "availabilityAlreadyExist": "Availability already added",
    "availabilityAlreadyExistChangeStartDate": "Availability already added please select another date or time",
    "availabilityEditSucess": "Availability is updated",
    "availabilityDelSucess": "Availability is deleted sucessfully",
    "availabilityFetchSucess": "Availability fetched ",
    "availabilityListSucess": "Availability list get sucess",
    "surveyContent": "\nThank you for being a patient of . Below you will find the link to start your survey. The survey should be easy and take very little time to fill out. This survey helps us save you time at the clinic so we can more efficiently process your paperwork and lessen your clinic waiting time.Please click below link to complete your survey.\nYou are one click away \n",
    "surveyIsStillPending": "your survey is still Pending. Please fill it before your appointment.\n Thank you for being a patient of this clinic. Below you will find the link to start your survey. The survey should be easy and take very little time to fill out. This survey helps us save you time at the clinic so we can more efficiently process your paperwork and lessen your clinic waiting time.Please click below link to complete your survey.\nYou are one click away \n",
    "addPatientPortalDemographics": "Patient portal demographic created sucessfully",
    "updatedPatientPortalDemographics": "Patient portal demographic updated sucessfully",
    "clinicIdAlreadyExist": "Clinic id already exist",
    "patientIdAlreadyExist": "Patient id already exist",
    "patientIdDosentExist": "Patient id dosen't exist",
    "patientDataInserted": "Patient data inserted",
    "clinicDemographicNotFound": "Clinic demographic settings not found",
    "updateMedicalHistory": "Medical history data updated",
    "deleteMedicalHistory": "Medical history data deleted",
    "updateDocument": "Document data updated",
    "deleteDocument": "Document data deleted",
    "slotUpdateSucess": "Slot updated sucessfully",
    "deleteMedicationHistory": "Medical history data deleted",
    "slotAlreadyBooked": "Slot is already booked",
    "slotFreeToBooked": "Slot is free for booking",
    "appointmentAlreadyExist": "Appointment already exist",
    "appointmentAdded": "Appointment added sucessfully",
    "appointmentUpdated": "Appointment update sucessfully",
    "appointmentListFetchedSucess": "Appointment list fetched sucessfully",
    "appointmentDeleted": "Appointment deleted sucessfully",

    //
    "subscriptionAddSuccess": "Subscription added sucessfully" ,
    "subscriptionListSuccess" : "Subscriptionlist fetched sucessfully" ,
    "subscriptionDeleteSuccess" : "Subscription deleted sucessfully" ,
    "updatesubscriptionSuccess" : "Subscription updated sucessfully",
    "listCardSuccess"           : "Card Listed sucessfully",
    "deleteCardSuccess"         : "Card Deleted Succssfully" ,
    "updateCardSuccess"          : "Card Updated Sucessfully" ,
    "CarddataFetchSuccess"       : "Card Data Fetch Successfully",
    "CouponPlanAssociated": "Coupon Plan Associated Successfully" ,


    //
    "cardupdateSuccess" : "Card updated sucessfully" ,
    "cancelSubscriptionSuccess" : "Subscription Cancelled sucessfully" ,
    "subscriptionUpdateSuccess" : "Subscription updated sucessfully" ,
    "newsubscriptionMesssage" : "New Subscription added Successfully" ,

    //optometry section 
    "getAnteriorSegmentSuccess" : "Anterior segment list sucessfully" ,
    "updateAnteriorSegmentSuccess" : "Anterior segment updated sucessfully" ,
    "addAnteriorSegmentMesssage" : "New anterior segment added Successfully",

    "getPosteriorSegmentSuccess" : "Posterior segment list sucessfully" ,
    "updatePosteriorSegmentSuccess" : "Posterior segment updated sucessfully" ,
    "addPosteriorSegmentMesssage" : "New Posterior segment added Successfully",

    "getChiefComplaintSuccess" : "Chief complaint list sucessfully" ,
    "updateChiefComplaintSuccess" : "Chief complaint updated sucessfully" ,
    "addChiefComplaintMesssage" : "New Chief complaint added Successfully",

    "getExternalSuccess" : "External list sucessfully" ,
    "updateExternalSuccess" : "External updated sucessfully" ,
    "addExternaltMesssage" : "New External added Successfully",

    "getOcularHistorySuccess" : "Ocular history list sucessfully" ,
    "updateOcularHistorySuccess" : "Ocular history updated sucessfully" ,
    "addOcularHistoryMesssage" : "New Ocular history added Successfully",

    "getPrescriptionsHistorySuccess" : "Prescriptions history list sucessfully" ,
    "updatePrescriptionsHistorySuccess" : "Prescriptions history updated sucessfully" ,
    "addPrescriptionsHistoryMesssage" : "New Prescriptions history added Successfully",

    "getPrescriptionSuccess" : "Prescription list sucessfully" ,
    "updatePrescriptionSuccess" : "Prescription updated sucessfully" ,
    "addPrescriptionMesssage" : "New Prescription added Successfully",

    

    //
    "deleteCoupounSuccess"         : "Coupon Deleted Succssfully" ,
    "coupounAddSuccess"             :"Coupon Added Successfully",
    "coupounAlreadyExist"             :"Coupon Already Exist  ",
    "assignCoupounsSucess"             : "Coupoun Assisgned Successfully" ,
    "CoupounAlreadyAssign"             : "Coupoun Already Assigned to User " ,
    "PlanAlreadyAssign"             : "Plan Has Already Been Assigned" ,
    "CouponMail"                     :"Mail Sent Successfully", 






    "addNewsLetters": "Newsletters data added",
    "addNewsLetterCategory": "Newsletter category data added",
    "updateNewsLetters": "Newsletter data updated",
    "deleteNewsLetters": "Newsletter data deleted",
    "deleteNewsLetterCategory": "Newsletter category data deleted",
    "updateNewsLetterCategory": "Newsletter category data updated",
    "updateNewsLettersTemplate": "Newsletter Template data updated",
    "deleteNewsLettersTemplate": "Newsletter Template data deleted",
    "dataNotFound": "Data not found",
    "addSections": "Patient portal sections data added",
    "updateSections": "Patient portal sections data updated",
    "deleteSections": "Patient portal sections data deleted",
    "addSectionsByClinic": "Patient portal sections data by clinic added",
    "updateSectionsByClinic": "Patient portal sections data by clinic updated",
    "status_required" : "Status field required."
}
const skills_message = {
    "skill_name": "Skill name is required",
    "skill_added_successfully":"Skill added successfully.",
    "skill_list_success":"Skills list fetched successfully.",
    "skill_updated_successfully":"Skill updated successfully",
    "status_updated":"Skill status updated successfully.",
    "status_deleted":"Skill deleted successfully.",
}
const casetype_message = {
    "casetype_name": "caseType is required",
    "casetype_added_successfully":"caseType added successfully.",
    "casetype_list_success":"caseTYpe list fetched successfully.",
    "casetype_updated_successfully":"caseType updated successfully",
    "casetype_status_updated":"caseType status updated successfully.",
    "casetype_status_deleted":"caseType deleted successfully.",
    "casetypeAlreadyExist": "caseType already exist",
}
const role_message = {
    "role_name": "Role is required",
    "role_added_successfully":"Role added successfully.",
    "role_list_success":"Role list fetched successfully.",
    "role_updated_successfully":"Role updated successfully",
    "role_status_updated":"Role status updated successfully.",
    "role_status_deleted":"Role deleted successfully.",
    "roleAlreadyExist": "Role already exist",
}
const amaQua_message = {
    "ama_name": "Qualification is required",
    "ama_added_successfully":"Qualification added successfully.",
    "ama_list_success":"Qualification list fetched successfully.",
    "ama_updated_successfully":"Qualification updated successfully",
    "ama_status_updated":"Qualification status updated successfully.",
    "ama_status_deleted":"Qualification deleted successfully.",
    "qualificationAlreadyExist": "Qualification already exist",
}
const validateMsg = {
    "emailAlreadyExist": "Email Id already exist, try with another",
    "passwordDoNotUseSame": "New password should not be same as old password",

    //User
    "userIdRequired": "User Id is required",
    "usernameAlreadyExist": "Username already exist, try with another",
    "emailRequired": "Email is required",
    "firstnameRequired": "First name is required",
    "passwordRequired": "Password is required",
    "deviceTypeRequired": "Device type is required",
    "deviceIdRequired": "Device id is required",
    "deviceTokenRequired": "Device token is required",
    "invalidEmail": "Invalid Email Given",
    "invalidEmailOrPassword": "Invalid email or password",
    "internalError": "Internal error",
    "requiredFieldsMissing": "Required fields missing",
    "emailNotExist": "Email doesn't exist",
    "userNotFound": "User not found",
    "passwordNotMatch": "New password should not be same as old password",
    "invalidDeviceType": "Invalid device type, It should be Android or iOS",
    "noRecordFound": "No record found",
    "noSlotFound": "No slot found",
    "invalidMongoId": "Invalid Id",
    "invalidProviderId": "Invalid Provider Id",
    


}

const emailKeyword = {
    "registration": "registration",

    "forgotPassword": "forgot_password",
    "forgot_password_app": "forgot_password_app",

    "clinicstaff": "clinic_staff",

    "assignsurvey": "assignsurvey",
    "confirmation": "confirmation",
    "deactivation": "deactivation",
    "contactus": "contactus",
    "newContentNotification": "newContentNotification",//template not created in database
    "surveypendingreminder": "surveypendingreminder",
    "appointmentnotificationPatient": "appointmentnotificationPatient",
    "appointmentnotificationPhysician": "appointmentnotificationPhysician",
    "couponcode" : "couponcode",
    "addUser" : 'addUser'

}

const contentNotification = {
    "getNewContentNotification": `\nWelcome to MDout ! \n Thanks so much for joining our communication and information portal platform. You will now be able to get notification of newly uploaded contents by us, receive important and informative emails. Please click below link to check our newly uploaded content. You are one click away... \n\n`
}

const emailSubjects = {
    "verify_email": "Welcome to the Case Management - Verify your email address ",
    "forgotPassword": "Case Management - Forgot password"
}
const admin = {
    "ID": "5bf2a8a328b76a524bfb0048",
}
const notify = {
    "caseMgmtTitle": "Case-Mgmt",
    "messageSent": "sent you a message"
}
const cryptoConfig = {
    "cryptoAlgorithm": "aes-256-ctr",
    "cryptoPassword": 'd6F3Efeq',
    "secret": "MDout",
}
const UserTypes = {
    clinic: "clinic",
    user: "user",
    staff: "staff"
}
const obj = {
    cryptoConfig: cryptoConfig,
    statusCode: statusCode,
    messages: messages,
    validateMsg: validateMsg,
    emailKeyword: emailKeyword,
    emailSubjects: emailSubjects,
    notify: notify,
    admin: admin,
    contentNotification: contentNotification,
    UserTypes: UserTypes,
    skills_message:skills_message,
    casetype_message:casetype_message,
    role_message:role_message,
    amaQua_message:amaQua_message,
};
module.exports = obj;
