'use strict';
/*
 * Utility - utility.js
 * Author: smartData Enterprises
 * Date: 15 th Oct 2018
 */
var constant = require('./constants');
var crypto = require('crypto'),
    algorithm = constant.cryptoConfig.cryptoAlgorithm,
    password = constant.cryptoConfig.cryptoPassword;
var fs = require("fs");
var path = require('path');
var async = require('async');
var mongoose = require('mongoose');
var User = mongoose.model('user');
const validator = require('./validator');
var nodemailer = require('nodemailer');
var utility = {};
var q = require('q');
const webPush = require('web-push');
const config = require('../config/config').get(process.env.NODE_ENV);
const uuidv1 = require('uuid/v1');

utility.sendmail = function (to, subject, userData, html, callback) {
    var smtpTransport = nodemailer.createTransport({
        service: 'GMAIL',
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'mdout123@gmail.com',
            pass: 'smart@mdout'
        }
    });


    var mailOptions = {
        to: userData.email,
        from: '"mdout123@gmail.com"',
        subject: subject,
        html: html
    };
    smtpTransport.sendMail(mailOptions, function (err) {
        if (err) {

            callback(err);
        } else {

            callback(null, true);
        }
    });
}

utility.dateToISOStringConvert = function (date) {
    var datearr = date.split("-");
    var dobj = new Date(parseInt(datearr[2]), parseInt(datearr[1]) - 1, parseInt(datearr[0]));
    date = dobj.toISOString();
    return date;
}

utility.getEncryptText = function (text) {
    var cipher = crypto.createCipher(algorithm, password);
    text = cipher.update(text, 'utf8', 'hex');
    text += cipher.final('hex');
    return text;
}

utility.getDecryptText = function (text) {
    var decipher = crypto.createDecipher(algorithm, password)
    var text = decipher.update(text, 'hex', 'utf8')
    text += decipher.final('utf8');
    return text;
}


utility.uploadImage = function (imageBase64, imageName, callback) {
    if (imageBase64 && imageName) {
        var timestamp = Number(new Date()); // current time as number
        var filename = +timestamp + '_' + imageName;
        var imagePath = "./public/assets/uploads/" + filename;
        fs.writeFile(path.resolve(imagePath), imageBase64, 'base64', function (err) {
            if (!err) {
                callback(config.webUrl + "/assets/uploads/" + filename);
            } else {
                callback(config.webUrl + "/assets/images/default-image.png");
            }
        });
    } else {
        callback(false);
    }
}

utility.fileExistCheck = function (path, callback) {
    fs.exists(path, function (err) {
        if (err) {
            callback(true);
        } else {
            callback(false);
        }
    });
}

utility.validationErrorHandler = function (err) {
    var errMessage = constant.validateMsg.internalError;
    if (err.errors) {
        for (var i in err.errors) {
            errMessage = err.errors[i].message;
        }
    }
    return errMessage;
}







utility.fileUpload = function (imagePath, buffer) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(path.resolve(imagePath), buffer, function (err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

utility.getSortObj = function (body) {
    var sorting = { createdAt: -1 };
    for (var key in body) {
        var reg = new RegExp("sorting", 'gi');
        if (reg.test(key)) {
            var value = body[key];
            key = key.replace(/sorting/g, '').replace(/\[/g, '').replace(/\]/g, '');
            var sorting = {};
            sorting[key] = (value == 'desc') ? 1 : -1;
        }
    }
    return sorting;
}


utility.getFilterObj = function (body) {
    var filter = {};
    for (var key in body) {
        var reg = new RegExp("filter", 'gi');
        if (reg.test(key)) {
            var value = body[key];
            key = key.replace(/filter/g, '').replace(/\[/g, '').replace(/\]/g, '');
            filter[key] = value;
        }
    }
    return filter;
}

utility.userValidation = function (body) {
    let requiredFieldsMissing = constant.validateMsg.requiredFieldsMissing;
    let invalidEmail = constant.validateMsg.invalidEmail;
    if (!body.email || !body.password) { //!body.firstname || !body.lastname || !body.role || !body.clinicId || !body.userType || !body.createdId 
        return requiredFieldsMissing;
    } else if (body.email && !validator.isEmail(body.email)) {
        return invalidEmail;
    } else {
        return true;
    }
}


utility.clinicValidation = function (body) {
    let requiredFieldsMissing = constant.validateMsg.requiredFieldsMissing;
    let invalidEmail = constant.validateMsg.invalidEmail;
    if (!body.clinicName) {
        return requiredFieldsMissing;
    } else if (body.email && !validator.isEmail(body.email)) {
        return invalidEmail;
    } else {
        return true;
    }
}

utility.staffValidation = function (body) {
    let requiredFieldsMissing = constant.validateMsg.requiredFieldsMissing;
    let invalidEmail = constant.validateMsg.invalidEmail;
    if (!body.firstname || !body.lastname || !body.clinicId || !body.userType || !body.createdId || !body.roleId) { //|| !body.roleId
        return requiredFieldsMissing;
    } else if (body.email && !validator.isEmail(body.email)) {
        return invalidEmail;
    } else {
        return true;
    }
}

utility.uuid = {
    uuid: require('node-uuid'),
    v1: function () {
        return this.uuid.apply(this, arguments);  //.v1()
    }
}

utility.getUuid = function () {
    return new Promise((resolve, reject) => {
        try {
            let newUuid = uuidv1();
            resolve(newUuid);
        } catch (error) {
        }
    })
}

/**
 * Function is used for push notification
 * @access private
 * @return json
 * @input  object which contains the following parameter 
 * title , message , url ,ttl ,icon,image,badge,tag
 * @to     array which contains to ids if empty will treated as all
 * Created by prakash kumar soni
 * @smartData Enterprises (I) Ltd
 * Created Date 13-Mar-2019
*/

// utility.pushNotification = function (input, to) {
//     return new Promise(function (resolve, reject) {
//         try {
//             const payload = {
//                 title: input.title ? input.title : "",
//                 message: input.message ? input.message : "",
//                 url: input.url ? input.url : "",
//                 ttl: input.ttl ? input.ttl : "",
//                 icon: input.icon ? input.icon : "",
//                 image: input.image ? input.image : "",
//                 badge: input.badge ? input.badge : "",
//                 tag: input.tag ? input.tag : "",
//             };
//             if (to && to.length > 0) {
//                 let i = 0;
//                 let len = to.length;
//                 for (i; i < len; i++) {
//                     sendNotification({ createdById: to[i] });
//                 }

//             } else {
//                 sendNotification({});
//             }
//             //obj of condition that subcription you want to send notification
//             function sendNotification(obj) {
//                 if (JSON.stringify(obj) !== '{}') {
//                     payload.for_id = obj.createdById
//                 }
//                 Subscription.find(obj, (err, subscriptions) => {
//                     if (err) {
//                         console.error("Error occurred while sending notification on finding subcription", err);
//                         return res.json(Response(500, constant.validateMsg.internalError, err));
//                     } else {
//                         let parallelSubscriptionCalls = subscriptions.map((subscription) => {
//                             return new Promise((resolve, reject) => {
//                                 const pushSubscription = {
//                                     endpoint: subscription.endpoint,
//                                     keys: {
//                                         p256dh: subscription.keys.p256dh,
//                                         auth: subscription.keys.auth
//                                     }
//                                 };

//                                 const pushPayload = JSON.stringify(payload);
//                                 const pushOptions = {
//                                     vapidDetails: {
//                                         subject: config.baseUrl,
//                                         privateKey: config.notify.privateKey,
//                                         publicKey: config.notify.publicKey
//                                     },
//                                     TTL: payload.ttl,
//                                     headers: {}
//                                 };
//                                 webPush.sendNotification(
//                                     pushSubscription,
//                                     pushPayload,
//                                     pushOptions
//                                 ).then((value) => {
//                                     resolve({
//                                         status: true,
//                                         endpoint: subscription.endpoint,
//                                         data: value
//                                     });
//                                 }).catch((err) => {
//                                     reject({
//                                         status: false,
//                                         endpoint: subscription.endpoint,
//                                         data: err
//                                     });
//                                 });
//                             });
//                         });
//                         q.allSettled(parallelSubscriptionCalls).then((pushResults) => {
//                             resolve({
//                                 status: true,
//                                 data: pushResults
//                             })
//                             console.info(pushResults);
//                         });
//                     }
//                 });
//             }
//         } catch (err) {
//             reject({
//                 status: false,
//                 data: err
//             });
//         }
//     })

// }
utility.getDateFromTimestamp = function getDateFromTimestamp(timestamp) {
    return new Date(timestamp*1000)
}

utility.today_date = function today_date() {
    return new Promise((resolve, reject) => {
        try {
            const currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
            const day = currentDate.getDate()
            const month = currentDate.getMonth() + 1
            const year = currentDate.getFullYear()
            const today_date = day + "-" + month + "-" + year;
            resolve(today_date);
        } catch (error) {
            reject('Someting worng with the date')
        }
       
    });
    
}

utility.check_date = function check_date(timestamp) {
    return new Promise((resolve, reject) => {
        try {
            const today_date = new Date(timestamp*1000);
            const day = today_date.getDate()
            const month = today_date.getMonth() + 1
            const year = today_date.getFullYear()
            const check_date = day + "-" + month + "-" + year;
            resolve(check_date);
        } catch (error) {
            reject('Someting worng with the date')
        }
       
    });
    
}

module.exports = utility;

