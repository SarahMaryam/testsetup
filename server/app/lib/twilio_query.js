'use strict';
var constant = require('./constants');
var fs = require("fs");
var twilioQuery = {};
var twilio = require('twilio');
const AccessToken = twilio.jwt.AccessToken;
const ChatGrant = AccessToken.ChatGrant;
var config = __rootRequire('app/config/config.js').get(process.env.NODE_ENV);
const twilioClient = require('twilio')(config.twiliovdemo.accountSid, config.twiliovdemo.authToken);

/**
 * Function is use to create a chat user
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 22 Feb 2019
*/
twilioQuery.createAChatUser = function createAChatUser(input) {
    var attributes = JSON.stringify(input.attributes);
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users
                .create({ identity: input.identity, attributes: attributes, friendlyName: input.friendlyName })
                .then((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })

}

/**
 * Function is use to retrive a chat user
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 28 Feb 2019
*/
twilioQuery.retriveChatUser = function retriveChatUser(input) {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users(input.userSid)
                .fetch()
                .then((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })

}

/**
 * Function is use to retrive a chat user list
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 01 Mar 2019
*/

twilioQuery.retriveChatUserList = function retriveChatUserList() {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users.list((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}


/**
 * Function is use to update a chat user
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 01 Mar 2019
*/


twilioQuery.updateChatUser = function updateChatUser() {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users(input.userSid)
                .update(input.dataToUpdate).then((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}


/**
 * Function is use to delete a chat user
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 01 Mar 2019
*/

twilioQuery.deleteChatUser = function deleteChatUser() {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users(input.userSid)
                .remove()
                .then((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}

/**
 * Function is use to view user channel list
 * @access private
 * @return json
 * Created by prakash soni
 * @smartData Enterprises (I) Ltd
 * Created Date 04 Mar 2019
*/

twilioQuery.viewUserChannelList = function viewUserChannelList(input) {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .users(input.userSid).userChannels.list((user) => {
                    if (user) {
                        tempObj.status = true;
                        tempObj.data = user;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}

twilioQuery.createChannel = function createChannel(input) {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .channels
                .create(input)
                .then(channel => {
                    if (channel) {
                        tempObj.status = true;
                        tempObj.data = channel;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}


twilioQuery.createMember = function createMember(input) {
    let tempObj = { "status": false, "data": [], "err": "" };
    return new Promise(function (resolve, reject) {
        try {
            twilioClient.chat.services(config.twiliovdemo.chatserviceSid)
                .channels(input.channelSid).members.create({ identity: input.user_id }).then(member => {
                    if (member) {
                        tempObj.status = true;
                        tempObj.data = member;
                        resolve(tempObj);
                    }
                }).catch(err => {
                    tempObj.status = false;
                    tempObj.err = err;
                    resolve(tempObj);
                });
        } catch (err) {
            reject(err);
        }
    })
}






module.exports = twilioQuery;
