'use strict';

var mongoose = require('mongoose');
var constantsObj = require('../../../../../lib/constants');

var userSchema = new mongoose.Schema({
    userName: { type: String, default: '' },
    email: { type: String, lowercase: true ,default : ''},  //unique: true,    
    password: { type: String, default: null },
   
}, {
        timestamps: true
    });

userSchema.statics.existCheck = function (email, id, callback) {
    var where = {};
    if (id) {
        where = {
            $or: [{ email: new RegExp('^' + email + '$', "i") }], deleted: { $ne: true }, _id: { $ne: id }
        };
    } else {
        where = { $or: [{ email: new RegExp('^' + email + '$', "i") }], deleted: { $ne: true } };
    }
    User.findOne(where, function (err, userdata) {
        if (err) {
            callback(err)
        } else {
            if (userdata) {
                callback(null, constantsObj.validateMsg.emailAlreadyExist);
            } else {
                callback(null, true);
            }
        }
    });
};

module.exports = mongoose.model('user', userSchema);

