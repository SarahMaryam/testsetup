﻿'use strict';

var util = require('util'),
    mongoose = require('mongoose'),
    jwt = require('jsonwebtoken'),
    db = __rootRequire('app/config/db'),
    async = require('async'),
    validator = __rootRequire('app/lib/validator'),
    utility = __rootRequire('app/lib/utility.js'),
    mailer = __rootRequire('app/lib/mailer.js'),
    Error = __rootRequire('app/lib/error.js'),
    Response = __rootRequire('app/lib/response.js'),
    constant = __rootRequire('app/lib/constants'),
    query = __rootRequire('app/lib/common_query'),
    helper = __rootRequire('app/config/helper'),
    User = mongoose.model('user'),
    Token = mongoose.model('token'),
    uniqid = require('uniqid');


const config = __rootRequire('app/config/config.js').get(process.env.NODE_ENV);

module.exports = {

    userRegister: userRegister
    
};

/**
 * Function is use to login user
 * @access private
 * @return json
 * Created by divya
 * @smartData Enterprises (I) Ltd
 * Created Date 6-Aug-2018
 */
function userRegister(req, res, next) {
    return true
    
}











